﻿using IServices.Auth;
using Microsoft.AspNetCore.Mvc;
using Model.Dto.Auth;
using System;
using System.Threading.Tasks;

namespace Core.Api.Controllers.Auth
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        private readonly IAccountService _accountService;

        public AccountsController(
            IAccountService accountService
            )
        {
            _accountService = accountService;
        }

        /// <summary>
        /// Identifica a un usuario
        /// </summary>
        /// <param name="userInfo">identifica a un usuario</param>
        /// <returns></returns>
        [HttpPost("Login")]
        public async Task<ActionResult<UserTokenDto>> Login([FromBody] UserAuthDto userInfo)
        {

            var userToken = await _accountService.Authenticate(userInfo);

            if (userToken != null)
            {
                return userToken;
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Usuario o contraseña incorrecta.");
                return BadRequest(ModelState);
            }
        }

        /// <summary>
        /// Registra un nuevo usuario
        /// </summary>
        /// <param name="userInfo"></param>
        /// <param name="url"></param>
        /// <returns></returns>
        [HttpPost("Register")]
        public async Task<ActionResult<UserTokenDto>> CreateUser([FromBody] UserDataCreateDto userInfo)
        {
            try
            {
                var ClientUrl = Request.Host;
                UrlVerificationDto url = new UrlVerificationDto() { Email = userInfo.Email, Urlbase = ClientUrl.ToString()};
                var userToken = await _accountService.RegisterUser(userInfo, url);

                if (userToken != null)
                {
                    return userToken;
                }
                else
                {
                    return BadRequest("Usuario o contraseña invalida");
                }

            }
            catch (Exception)
            {
                return BadRequest();
            }
        }


    }
}
