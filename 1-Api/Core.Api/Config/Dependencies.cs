﻿using IRepository;
using IServices.Auth;
using IServices.Email;
using IUnitofWork;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Repository;
using Services.Auth;
using Services.Email;
using UnitofWork;

namespace Core.Api.Config
{
    public static class Dependencies
    {
        public static void AddMyDependencies(
            this IServiceCollection services,
            IConfiguration configuration
            )
        {
            #region Services
            services.AddScoped<IUnitOfWorkContainer, UnitOfWorkContainer>();
            services.AddScoped<IUnitOfWorkRepository, UnitOfWorkRepository>();

            //repository

            // services
            services.AddScoped<IApplicationUserRepository,ApplicationUserRepository>();
            services.AddScoped<IEmailSenderService, EmailSenderService>();
            services.AddScoped<IEmailAccountService, EmailAccountService>();
            services.AddScoped<IAccountService, AccountService>();
            #endregion
        }
    }
}
