﻿
using AutoMapper;
using Model.Dto.Auth;
using Model.Entities.Auth;

namespace Core.Api.Config
{
    public class MyMaps : Profile
    {
        public MyMaps()
        {
            #region usuario
            CreateMap<ApplicationUser, UserDataDto>().ReverseMap();
            CreateMap<ApplicationUser, UserDataCreateDto>().ReverseMap();
            #endregion
        }
    }
}
