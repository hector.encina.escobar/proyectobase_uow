﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Model.Entities.Auth;
using Persistence.DatabaseContext.Config.User;

namespace Persistence.DatabaseContext
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        #region Dbsets
        #endregion

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
               : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelbuilder)
        {
            base.OnModelCreating(modelbuilder);

            #region fluent api contraints
            new UserConfig(modelbuilder.Entity<ApplicationUser>());
            #endregion

            #region nombre tablas de identity personalizadas
            modelbuilder.Entity<ApplicationUser>(b =>
            {
                b.ToTable("Users", "dbo");
            });

            modelbuilder.Entity<IdentityUserClaim<string>>(b =>
            {
                b.ToTable("UserClaims", "dbo");
            });

            modelbuilder.Entity<IdentityUserLogin<string>>(b =>
            {
                b.ToTable("UserLogins", "dbo");
            });

            modelbuilder.Entity<IdentityUserToken<string>>(b =>
            {
                b.ToTable("UserTokens", "dbo");
            });

            modelbuilder.Entity<IdentityRole>(b =>
            {
                b.ToTable("Roles", "dbo");
            });

            modelbuilder.Entity<IdentityRoleClaim<string>>(b =>
            {
                b.ToTable("RoleClaims", "dbo");
            });

            modelbuilder.Entity<IdentityUserRole<string>>(b =>
            {
                b.ToTable("UserRoles", "dbo");
            });
            #endregion
        }



    }
}
