﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Model.Entities.Auth;

namespace Persistence.DatabaseContext.Config.User
{
    public class UserConfig
    {
        public UserConfig(EntityTypeBuilder<ApplicationUser> entityBuilder)
        {
            entityBuilder.Property(x => x.FirstName).IsRequired().HasMaxLength(50);
            entityBuilder.Property(x => x.PrimaryLastName).IsRequired().HasMaxLength(50);
            entityBuilder.Property(x => x.SecondLastName).HasMaxLength(50);
        }
    }
}
