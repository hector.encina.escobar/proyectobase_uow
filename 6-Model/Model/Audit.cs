﻿using Model.Entities.Auth;
using System;

namespace Model
{
    public interface IAudit
    {
        bool IsDeleted { get; set; }
        DateTime CreateDate { get; set; }
        DateTime UpdateDate { get; set; }
        DateTime DeletedDate { get; set; }
        //string UserUpdateId { get; set; }
        //string UserDeletedId { get; set; }
    }

    public class Audit : IAudit
    {
        public bool IsDeleted { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set ; }
        public DateTime DeletedDate { get; set ; }
        //public string UserUpdateId { get; set; }
        //public string UserDeletedId { get; set; }

        //public ApplicationUser UserUpdate { get; set; }
        //public ApplicationUser UserDeleted { get; set; }


    }
}
