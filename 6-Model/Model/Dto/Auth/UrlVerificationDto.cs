﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Dto.Auth
{
    public class UrlVerificationDto
    {
        public string Email { get; set; }
        public string Urlbase { get; set; }
    }
}
