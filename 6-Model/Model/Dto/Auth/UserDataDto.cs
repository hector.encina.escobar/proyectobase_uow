﻿
using System.Collections.Generic;

namespace Model.Dto.Auth
{
    public class UserDataDto
    {
        public string FirstName { get; set; }
        public string PrimaryLastName { get; set; }
        public string SecondLastName { get; set; }
        public string Email { get; set; }
        public IList<string> Roles { get; set; }


        // public string ApplicationUserId { get; set; }
    }
}
