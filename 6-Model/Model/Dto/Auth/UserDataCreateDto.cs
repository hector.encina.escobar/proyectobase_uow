﻿
namespace Model.Dto.Auth
{
    public class UserDataCreateDto
    {
        public string FirstName { get; set; }
        public string PrimaryLastName { get; set; }
        public string SecondLastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
