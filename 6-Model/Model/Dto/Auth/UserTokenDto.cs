﻿using System;

namespace Model.Dto.Auth
{
    public class UserTokenDto
    {
        public string Token { get; set; }
        public DateTimeOffset Expiration { get; set; }
        public UserDataDto User { get; set; }
    }
}
