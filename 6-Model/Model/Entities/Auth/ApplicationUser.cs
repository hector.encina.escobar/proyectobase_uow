﻿using Microsoft.AspNetCore.Identity;
using System;

namespace Model.Entities.Auth
{
    public class ApplicationUser: IdentityUser, IAudit
    {
        public string FirstName { get; set; }
        public string PrimaryLastName { get; set; }
        public string SecondLastName { get; set; }
        public string PathImage { get; set; }

        // propiedades auditoria
        public bool IsDeleted { get; set; }
        public DateTime CreateDate { get; set ; }
        public DateTime UpdateDate { get; set; }
        public DateTime DeletedDate { get; set; }
    }
}
