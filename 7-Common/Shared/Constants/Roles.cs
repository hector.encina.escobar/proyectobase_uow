﻿
namespace Shared.Constants
{
    public static class Roles
    {
        public const string ADMIN = "admin";
        public const string USER = "usuario";
        public const string INVITED = "invitado";
    }
}
