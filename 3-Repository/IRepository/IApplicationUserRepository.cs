﻿using IRepository.Actions;
using Microsoft.AspNetCore.Identity;
using Model.Entities.Auth;
using System.Threading.Tasks;

namespace IRepository
{
    public interface IApplicationUserRepository : IPagedRepository<ApplicationUser>, ICreateRepository<ApplicationUser>, IReadRepository<ApplicationUser>, IRemoveRepository<ApplicationUser>, IUpdateRepository<ApplicationUser>
    {

    }
}
