﻿using IRepository;
using Microsoft.AspNetCore.Identity;
using Microsoft.Data.SqlClient;
using Model.Entities.Auth;
using Persistence.DatabaseContext;
using Shared.Constants;
using System.Threading.Tasks;

namespace Repository
{
    public class ApplicationUserRepository : RepositoryBase<ApplicationUser, ApplicationDbContext>, IApplicationUserRepository
    {
        public ApplicationUserRepository(
            ApplicationDbContext context
            )
        {
            _context = context;
        }
    }
}
