﻿using IRepository;
using IUnitofWork;
using Microsoft.AspNetCore.Identity;
using Model.Entities.Auth;
using Persistence.DatabaseContext;
using Repository;
using System;
using System.Data.SqlClient;

namespace UnitofWork
{
    public class UnitOfWorkRepository : IUnitOfWorkRepository
    {
        private  ApplicationDbContext _context;
        public UserManager<ApplicationUser> UserManager { get; }
        public  SignInManager<ApplicationUser> SignInManager { get; }
        public  RoleManager<IdentityRole> RoleManager { get; }
        public IApplicationUserRepository ApplicationUserRepository { get; }
        public UnitOfWorkRepository(ApplicationDbContext context,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            RoleManager<IdentityRole> roleManager)
        {
            _context = context;
            UserManager = userManager;
            SignInManager = signInManager;
            RoleManager = roleManager;
            ApplicationUserRepository = new ApplicationUserRepository(_context);
        }
    }
}
