﻿using IUnitofWork;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Storage;
using Model.Entities.Auth;
using Persistence.DatabaseContext;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace UnitofWork
{
    public class UnitOfWorkContainer : IUnitOfWorkContainer
    {
        private readonly ApplicationDbContext _context;
        public IUnitOfWorkRepository Repository { get; }

        public UnitOfWorkContainer(ApplicationDbContext context,
            IUnitOfWorkRepository repository
            )
        {
            _context = context;
            Repository = repository;
        } 

        #region Transactions
        public IDbContextTransaction BeginTransaction()
        {
            return _context.Database.BeginTransaction();
        }

        public async Task<IDbContextTransaction> BeginTransactionAsync()
        {
            return await _context.Database.BeginTransactionAsync();
        }

        public void CommitTransaction()
        {
            _context.Database.CommitTransaction();
        }

        public void RollbackTransaction()
        {
            _context.Database.RollbackTransaction();
        }

        #endregion Transactions

        public void DetectChanges()
        {
            _context.ChangeTracker.DetectChanges();
        }


        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
