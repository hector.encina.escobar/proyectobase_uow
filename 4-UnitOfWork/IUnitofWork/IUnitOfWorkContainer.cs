﻿using Microsoft.EntityFrameworkCore.Storage;
using System.Threading.Tasks;

namespace IUnitofWork
{
    public interface IUnitOfWorkContainer
    {
        void DetectChanges();
        void SaveChanges();
        Task SaveChangesAsync();
        IDbContextTransaction BeginTransaction();
        Task<IDbContextTransaction> BeginTransactionAsync();
        void CommitTransaction();
        void RollbackTransaction();

        IUnitOfWorkRepository Repository { get; }
    }
}
