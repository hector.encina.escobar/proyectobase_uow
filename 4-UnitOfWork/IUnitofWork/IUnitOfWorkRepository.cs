﻿using IRepository;
using Microsoft.AspNetCore.Identity;
using Model.Entities.Auth;
using System;

namespace IUnitofWork
{
    public interface IUnitOfWorkRepository
    {
        public  UserManager<ApplicationUser> UserManager { get; }
        public  SignInManager<ApplicationUser> SignInManager { get; }
        public  RoleManager<IdentityRole> RoleManager { get; }
        public IApplicationUserRepository ApplicationUserRepository { get; }
    }
}
