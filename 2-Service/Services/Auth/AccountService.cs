﻿using AutoMapper;
using IRepository;
using IServices.Auth;
using IUnitofWork;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Model.Dto.Auth;
using Model.Entities.Auth;
using Shared.Constants;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Services.Auth
{
    public class AccountService:  ServiceBase<ApplicationUser>, IAccountService
    {
        private readonly IEmailAccountService _emailAccountService;
        private readonly IConfiguration _configuration;

        public AccountService(
            IUnitOfWorkContainer unitOfWork,
            IEmailAccountService emailAccountService,
            IConfiguration configuration,
            IMapper mapper
            ) : base(unitOfWork, mapper)
        {
            _emailAccountService = emailAccountService;
            _configuration = configuration;
        }


        public async Task<UserTokenDto> Authenticate(UserAuthDto userInfo)
        {
            var result = await _uow.Repository.SignInManager.PasswordSignInAsync(userInfo.Email, userInfo.Password, false, false);
            if (result.Succeeded)
            {
                var user = await _uow.Repository.UserManager.FindByEmailAsync(userInfo.Email);
                var roles = await _uow.Repository.UserManager.GetRolesAsync(user);
                return BuildToken(userInfo, roles);

            }
            else
            {
                return null;
                // throw new NotImplementedException();
            }
         }

        public async Task<UserTokenDto> RegisterUser(UserDataCreateDto model, UrlVerificationDto verificationUrl)
        {
            await _uow.BeginTransactionAsync();
            var new_user = _mapper.Map<ApplicationUser>(model);
            var result = await _uow.Repository.UserManager.CreateAsync(new_user, model.Password);

            if (result.Succeeded)
            {
                var roles = new List<string> { Roles.INVITED };
                await _emailAccountService.SendEmailForConfirmation(new_user, verificationUrl);
                UserAuthDto auth_user = _mapper.Map<UserAuthDto>(model);
                UserTokenDto token = BuildToken(auth_user, roles);
                _uow.CommitTransaction();
                return token;
            }
            else
            {
                _uow.RollbackTransaction();
                // result.Errors;
                return null;
                // throw new NotImplementedException();
            }
        }

        private UserTokenDto BuildToken(UserAuthDto userInfo, IList<string> roles)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.UniqueName, userInfo.Email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            foreach (var rol in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, rol));
            }

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["SecretKey"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            // Tiempo de expiración del token:
            var expiration = DateTime.UtcNow.AddHours(1);

            JwtSecurityToken token = new JwtSecurityToken(
               issuer: null,
               audience: null,
               claims: claims,
               expires: expiration,
               signingCredentials: creds);


            return new UserTokenDto()
            {
                Token = new JwtSecurityTokenHandler().WriteToken(token),
                Expiration = expiration,
            };
        }
    }
}
