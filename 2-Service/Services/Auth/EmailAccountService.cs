﻿using IServices.Auth;
using IServices.Email;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.WebUtilities;
using Model.Dto.Auth;
using Model.Entities.Auth;
using System;
using System.Text;
using System.Threading.Tasks;

namespace Services.Auth
{
    public class EmailAccountService : IEmailAccountService
    {

        private readonly UserManager<ApplicationUser> _userManager;

        private readonly IEmailSenderService _emailSenderService;

        public EmailAccountService (UserManager<ApplicationUser> userManager, IEmailSenderService emailSenderService)
        {
            _userManager = userManager;
            _emailSenderService = emailSenderService;
        }

        /// <summary>
        /// Envia email de recuperación de contraseña
        /// </summary>
        /// <param name="verificationUrl"></param>
        /// <returns></returns>
        public async Task<bool> EmailRecoverPassword(UrlVerificationDto verificationUrl)
        {
            bool result = false;
            try
            {
                var user = await _userManager.FindByEmailAsync(verificationUrl.Email);

                if (user != null)
                {
                    // genera token
                    string token = await _userManager.GeneratePasswordResetTokenAsync(user);

                    byte[] tokenGeneratedBytes = Encoding.UTF8.GetBytes(token);
                    var codeEncoded = WebEncoders.Base64UrlEncode(tokenGeneratedBytes);


                    var url = $"{verificationUrl.Urlbase}/{user.Id}/{codeEncoded}";
                    await _emailSenderService.SendEmailAsync(
                        user.Email,
                        "Recuperación contraseña My APP",
                        $"Estimado/a <span style='font-weight:bold'>{user.FirstName} {user.PrimaryLastName} {user.SecondLastName} <span><span style='font-weight:normal'>:</span>" +
                        "<p> </p>" +
                        "<p><span style='font-size:10.0pt;font-family:Arial;font-weight:normal;'>" +
                        "Para restablecer su contraseña ingrese al siguiente enlace: <o:p></o:p></span></p>" +
                        "<br>" +
                        $"<a href='{url}'>" + "Restablecer contraseña<a>" +
                        "<p> </p>" +
                        "Atentamente"
                        );
                    result = true;
                }
                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<bool> InformsVerifiedEmailUser(string userId, string token)
        {
            bool result = false;

            try
            {

                var codeDecodedBytes = WebEncoders.Base64UrlDecode(token);
                var codeDecoded = Encoding.UTF8.GetString(codeDecodedBytes);

                ApplicationUser user = await _userManager.FindByIdAsync(userId);
                IdentityResult res = await _userManager.ConfirmEmailAsync(user, codeDecoded);

                if (res.Succeeded)
                {
                    await _emailSenderService.SendEmailAsync(
                             user.Email,
                             "Email verificado - My APP",
                             $"Estimado/a <span style='font-weight:bold'>{user.FirstName} {user.PrimaryLastName} {user.SecondLastName} <span><span style='font-weight:normal'>:</span>" +
                             "<p> </p>" +
                             "<p><span style='font-size:10.0pt;font-family:Arial;font-weight:normal;'>" +
                             "Desde ya le damos la bienvenida y le informamos que su email ha sido verificado correctamente, sin embargo el módulo correspondiente a su rol se activará una vez que verifiquemos sus datos. <o:p></o:p></span></p>" +
                             "<br>" +
                             "<p> </p>" +
                             "My APP"
                             );

                    result = true;
                }

            }
            catch (Exception)
            {

                throw;
            }

            return result;
        }


        public async Task<bool> RequestEmailForConfirmation(UrlVerificationDto verificationUrl)
        {
            ApplicationUser user = await _userManager.FindByEmailAsync(verificationUrl.Email);
            return await SendEmailForConfirmation(user, verificationUrl);
        }


        public async Task<bool> SendEmailForConfirmation(ApplicationUser user, UrlVerificationDto verificationUrl)
        {
            bool result = false;
            try
            {
                if (user != null)
                {
                    // genera token
                    string token = await _userManager.GenerateEmailConfirmationTokenAsync(user);

                    byte[] tokenGeneratedBytes = Encoding.UTF8.GetBytes(token);
                    var codeEncoded = WebEncoders.Base64UrlEncode(tokenGeneratedBytes);

                    var url = $"{verificationUrl.Urlbase}/{user.Id}/{codeEncoded}";
                    await _emailSenderService.SendEmailAsync(
                        verificationUrl.Email,
                        "Confirme su email - My APP",
                        $"Estimado/a <span style='font-weight:bold'>{user.FirstName} {user.PrimaryLastName} {user.SecondLastName} <span><span style='font-weight:normal'>:</span>" +
                        "<p> </p>" +
                        "<p><span style='font-size:10.0pt;font-family:Arial;font-weight:normal;'>" +
                        "Bienvenido a la bitácora de My APP, para completar su registro verifique su email en el siguiente enlace: <o:p></o:p></span></p>" +
                        "<br>" +
                        $"<a href='{url}'>" + "Verificar email<a>" +
                        "<p> </p>" +
                        "Atentamente"
                        );
                    result = true;
                }
            }
            catch (Exception)
            {

                throw;
            }
            return result;
        }
    }
}
