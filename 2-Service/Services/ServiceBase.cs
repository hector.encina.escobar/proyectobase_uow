﻿using AutoMapper;
using IUnitofWork;
using Model;

namespace Services
{
    public abstract class ServiceBase<TEntity>
        where TEntity : class, IAudit
    {
        protected readonly IUnitOfWorkContainer _uow;
        protected readonly IMapper _mapper;

        // protected readonly IUnitOfWork _uow;

        public ServiceBase(
            IUnitOfWorkContainer unitOfWorkContainer,
            IMapper mapper)
        {
            _uow = unitOfWorkContainer;
            _mapper = mapper;
        }
    }
}
