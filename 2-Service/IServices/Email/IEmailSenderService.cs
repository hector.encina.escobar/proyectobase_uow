﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IServices.Email
{
    public interface IEmailSenderService
    {
        Task SendEmailAsync(string email, string subject, string htmlMessage);
    }
}
