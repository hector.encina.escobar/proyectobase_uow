﻿using Model.Dto.Auth;
using Model.Entities.Auth;
using System.Threading.Tasks;

namespace IServices.Auth
{
    public interface IEmailAccountService
    {
        Task<bool> RequestEmailForConfirmation(UrlVerificationDto verificationUrl);
        Task<bool> SendEmailForConfirmation(ApplicationUser user, UrlVerificationDto verificationUrl);
        Task<bool> InformsVerifiedEmailUser(string userId, string token);
        Task<bool> EmailRecoverPassword(UrlVerificationDto verificationUrl);
    }
}