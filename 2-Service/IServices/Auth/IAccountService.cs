﻿using Model.Dto.Auth;
using System.Threading.Tasks;

namespace IServices.Auth
{
    public interface IAccountService
    {
        Task<UserTokenDto> RegisterUser(UserDataCreateDto model, UrlVerificationDto verificationUrl);
        Task<UserTokenDto> Authenticate(UserAuthDto userInfo);

        // recuperar contraseña
        // cambiar contraseña
        // actualizar datos usuario
    }
}
